﻿using MahApps.Metro.Controls;
using RestSharp;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using TaskListWpf.WebApiHandler;

namespace TaskListWpf
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : MetroWindow
    {

        private ViewModel viewModel = new ViewModel();
        private ApiHandler api = new ApiHandler();

        public MainWindow()
        {
            InitializeComponent();

            DataContext = viewModel;

            viewModel.User = api.GetUserById(1);
            viewModel.Users = api.GetUsers();
            viewModel.Tasks = api.GetTasks();
            viewModel.DeadLine = DateTime.Now;

            TasksList.ItemsSource = api.GetTasks();
        }

        public void TaskButtonEvt(object sender, RoutedEventArgs e)
        {
            int id = int.Parse(((UIElement)sender).Uid);
            viewModel.Task = api.GetTaskById(id);
        }

        public void CreateTask(object sender, RoutedEventArgs e)
        {
            WebApiHandler.Task task = new WebApiHandler.Task
            {
                User = viewModel.User,
                AssignedUser = viewModel.AssignedUser,
                Title = viewModel.Title,
                Content = viewModel.Content,
                DeadLine = viewModel.DeadLine
            };

            if (api.CreateTask(task))
                TasksList.ItemsSource = api.GetTasks();
        }

        public void Filter(object sender, RoutedEventArgs e)
        {
            TasksList.ItemsSource = api.GetTasks()
                .Where(t => t.AssignedUser.Id == viewModel.SelectedUser.Id);
        }

        public void ClearFilter(object sender, RoutedEventArgs e)
        {
            TasksList.ItemsSource = api.GetTasks();            
        }

        public void FilterDone(object sender, RoutedEventArgs e)
        {
            switch (((UIElement)sender).Uid)
            {
                case "btn_done":
                    TasksList.ItemsSource = api.GetTasks()
                                .Where(t =>
                                    t.Done == true &&
                                    (viewModel.SelectedUser != null ?
                                    viewModel.SelectedUser.Id == t.AssignedUser.Id :
                                    true)
                                );
                    break;
                case "btn_open":
                    TasksList.ItemsSource = api.GetTasks()
                                .Where(t =>
                                    t.Done == false &&
                                    (viewModel.SelectedUser != null ?
                                    viewModel.SelectedUser.Id == t.AssignedUser.Id :
                                    true)
                                );
                    break;
                default:
                    TasksList.ItemsSource = api.GetTasks()
                               .Where(t =>
                                    (viewModel.SelectedUser != null ?
                                    viewModel.SelectedUser.Id == t.AssignedUser.Id :
                                    true)
                                );
                    break;
            }
        }

        public void MarkAsDone(object sender, RoutedEventArgs e)
        {
            api.UpdateTask(viewModel.Task, "done", "TRUE");
        }
    }

    class ViewModel : INotifyPropertyChanged
    {        
        public string Title { get; set; }
        public string Content { get; set; }
        public User User { get; set; }
        public User AssignedUser { get; set; }
        public User SelectedUser { get; set; }
        public WebApiHandler.Task Task { get; set; }
        public List<User> Users { get; set; }
        public List<WebApiHandler.Task> Tasks { get; set; }
        public DateTime DeadLine { get; set; }

        public event PropertyChangedEventHandler PropertyChanged;
    }

}

