﻿using RestSharp;
using System;
using System.Collections.Generic;
using System.Text;

namespace TaskListWpf.WebApiHandler
{
    class ApiHandler
    {

        private RestClient client = new RestClient("http://clearftp.cba.pl/tasklist_api/api/");

        public List<User>? GetUsers()
        {
            var request = new RestRequest("users.php");

            var response = client.Get<UserResponse>(request);

            if (response.IsSuccessful)
            {
                return response.Data.Users;
            }
            else return null;
        }

        public User GetUserById(int id)
        {
            var request = new RestRequest("users.php")
                .AddParameter("id", id);

            var response = client.Get<UserResponse2>(request);

            if (response.IsSuccessful)
            {
                return response.Data.User;
            }
            else return null;
        }

        public List<Task>? GetTasks()
        {
            var request = new RestRequest("tasks.php");

            var response = client.Get<TaskResponse>(request);

            if (response.IsSuccessful)
            {
                return response.Data.Tasks;
            }
            else return null;
        }

        public Task GetTaskById(int id)
        {
            var request = new RestRequest("tasks.php")
                .AddParameter("id", id);

            var response = client.Get<TaskResponse2>(request);

            if (response.IsSuccessful)
            {
                return response.Data.Task;
            }
            else return null;
        }

        public bool CreateTask(Task task)
        {
            var request = new RestRequest("task/create.php")
                .AddParameter("userId", task.User.Id)
                .AddParameter("assignedUserId", task.AssignedUser.Id)
                .AddParameter("title", task.Title)
                .AddParameter("content", task.Content)
                .AddParameter("deadLine", task.DeadLine.ToString("yyyy'-'MM'-'dd' 'HH':'mm':'ss"));

            var response = client.Post(request);

            return response.IsSuccessful;
        }

        public bool UpdateTask(Task task, string property, string value)
        {
            var request = new RestRequest("task/update.php")
                .AddParameter("id", task.Id)
                .AddParameter("property", property)
                .AddParameter("value", value);

            var response = client.Patch(request);

            return response.IsSuccessful;
        }
    }

    class UserResponse
    {
        public List<User> Users { get; set; }
    }

    class UserResponse2
    {
        public User User { get; set; }
    }

    class User
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Image { get; set; }
    }

    class TaskResponse
    {
        public List<Task> Tasks { get; set; }
    }

    class TaskResponse2
    {
        public Task Task { get; set; }
    }

    class Task
    {     
        
        public int Id { get; set; }
        public User User { get; set; }
        public User AssignedUser { get; set; }        
        public string Title { get; set; }
        public string Content { get; set; }
        public string CreatedAt { get; set; }
        public DateTime DeadLine { get; set; }
        public bool Done { get; set; }
    }

}
